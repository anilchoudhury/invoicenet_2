FROM ubuntu:18.04
USER root
RUN mkdir /InvoiceNet
WORKDIR /InvoiceNet
COPY . /InvoiceNet
WORKDIR /InvoiceNet
RUN apt update &&  apt -y install software-properties-common python3.8
RUN add-apt-repository -y ppa:deadsnakes/ppa
RUN apt update & apt-get install -y pyqt5-dev-tools python3-pip
RUN echo $(python3 --version)
RUN apt update
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Asia/Kolkata
RUN apt-get install -y python3-tk poppler-utils  libleptonica-dev tesseract-ocr libtesseract-dev libmagickwand-dev 
RUN apt update & python3 -m pip install pytesseract tensorflow==2.0.0b1 datefinder tqdm numpy simplejson pdf2image matplotlib PyPDF2 magickwand Ghost.py pdfplumber --user gast==0.2.2
CMD ["pytesseract.pytesseract.tesseract_cmd", "r'/usr/bin/tesseract'"]
CMD  ./install.sh
RUN apt update && apt-get install -y python3-venv 
CMD ["python3", "-m", "venv", "env"]
CMD ["source", "env/bin/activate"]
#RUN raja=echo $(hostname -I | awk '{print $1}')
#RUN echo $raja
RUN apt-get update && apt-get install -y openssh-client
#CMD 
#CMD ["/bin/bash"]
#CMD ["/bin/bash", "-X"]
#CMD ["python3", "trainer.py", "-X"]
#ENTRYPOINT ["python3", "trainer.py"]

#CMD ["python3", "prepare_data.py", "--data_dir", "train_data/"]
#CMD ["python3", "train.py", "--field", "email", "--batch_size", "8"]
#CMD ["python3", "predict.py", "--field", "email", "--invoice", "train_data/invoice_4.pdf"]
USER root
EXPOSE 8080
CMD ["python3", "welcome.py"]

#CMD ["python3", "trainer.py"]
#CMD ["python3", "extractor.py"]
#CMD ["/bin/bash", "-X"]
#USER 1001

